import axios from "axios";

export const api = axios.create({
  baseURL: "https://pokedex20201.herokuapp.com",
  // baseURL: 'https://pokedex-cjr.herokuapp.com',
  headers: { "Content-Type": "application/json" },
});

export const catchEmAll = (page) => api.get("/pokemons.json?page=" + page);
export const analyse = (pokemon) => api.get("/pokemons/" + pokemon);
export const signUp = (username) => api.post("/users", { username: username });
export const getTrainer = (username) => api.get("/users/" + username);
export const star = (username, pokemon, op) => {
  const url = "/users/" + username + "/starred/" + pokemon;
  return op ? api.delete(url) : api.post(url);
};
