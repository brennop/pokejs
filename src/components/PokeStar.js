import React from 'react';
import styled from 'styled-components';
import {TiStarFullOutline, TiStarOutline} from 'react-icons/ti';

const Icon = icon => styled(icon)`
    color: gold;
    cursor: pointer;
    position: absolute;
    top: ${props => props.top};
    right: ${props => props.right};
    bottom: ${props => props.bottom};
    left: ${props => props.left};
    font-size: ${props => props.fontSize || "1rem"};
    transition: 0.1s;
    vertical-align: top;

    &:active {
      transition: 0.2s ease-out;
      transform: scale(0.85);
    }
`;

const StarFull = Icon(TiStarFullOutline);
const Star = Icon(TiStarOutline);

class PokeStar extends React.Component {
  render() {
    return this.props.starred ? <StarFull {...this.props}/> : <Star {...this.props}/>;
  }
}

export default PokeStar;
