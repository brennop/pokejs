import styled, {keyframes} from 'styled-components';
import {ReactComponent as PokeBallIcon} from '../images/pokeball.svg';

const spin = keyframes`
  100% { transform: rotate(360deg) }
`;

const PokeBall = styled(PokeBallIcon)`
  fill: #b8b8b8;
  margin: 2em;
  height: 2em;
  animation: ${spin} 1s linear infinite;
`;

export default PokeBall;
