import React from 'react';
import styled from 'styled-components';
import Modal from 'react-bootstrap/Modal';

const StyledModal = styled(Modal)`
  /* Applies style to modal box */
  & .modal-content {
    border: 0;
    border-radius: 12px;
    padding: 0.4em;
    background: ${props => props.theme.body};
    color: ${props => props.theme.text};
  }
`;

class PokeModal extends React.Component {
  render() {
    return (
      <StyledModal
        show={this.props.show}
        onHide={this.props.onHide}
        centered
        animation={true}>
        <Modal.Body>{this.props.children}</Modal.Body>
      </StyledModal>
    );
  }
}

export default PokeModal;
