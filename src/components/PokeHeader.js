import React from 'react';
import styled, {withTheme} from 'styled-components';
import Red from '../images/red.png';
import {Location, navigate} from '@reach/router';
import {FaMoon, FaRegMoon} from 'react-icons/fa';

const Container = styled.div`
  font-family: 'Barlow', sans-serif;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 2rem;
  background: ${props => props.theme.body};
  color: ${props => props.theme.text};
  transition: 0.2s;
`;

const Right = styled.div`
  display: flex;
  align-items: center;
`;

const Title = styled.h1`
  font-weight: 900;
  cursor: pointer;
`;

const Line = styled.hr`
  background-color: ${props => props.theme.highlight};
  transition: 0.2s;
  border: none;
  height: 1px;
  margin: 0;
`;

const User = styled.div`
  border-radius: 100%;
  font-weight: 900;
  font-size: 2.4rem;
  background: white;

  display: flex;
  align-items: center;
  justify-content: center;

  border: 1px solid #00000040;
  overflow: hidden;

  height: 48px;
  width: 48px;

  cursor: pointer;
  color: #232323; 

  background-image: ${props => props.username && `url(${Red})`};
  background-size: cover;
  background-position: 50% calc(50% + 4px);
  background-repeat: no-repeat;
`;

const Moon = styled.div`
  cursor: pointer;
  margin: 20px;
`;

class Header extends React.Component {
  state = {
    username: localStorage.getItem('user'),
  };

  render() {
    return (
      <>
        <Container>
          <Title onClick={() => navigate('/')}>Pokédex</Title>
          <Right>
            <Moon onClick={this.props.toggleTheme}>
              {this.props.theme.name === 'light' ? <FaRegMoon /> : <FaMoon />}
            </Moon>
            <Location>
              {({location}) => (
                <User
                  onClick={() =>
                    navigate('/user', {
                      state: {
                        oldLocation: JSON.parse(JSON.stringify(location)),
                      },
                    })
                  }
                  username={this.state.username == null}>
                  {this.state.username && this.state.username.charAt(0)}
                </User>
              )}
            </Location>
          </Right>
        </Container>
        <Line />
        {this.props.children}
      </>
    );
  }
}

export default withTheme(Header);
