import React from "react";
import styled, { keyframes } from "styled-components";
import { Location, navigate } from "@reach/router";
import { gradient } from "../utils/colors";
import PokeStar from "./PokeStar";
import { star } from "../api";
import { fadeIn } from "react-animations";

const fadeAnimation = keyframes`${fadeIn}`;

const Card = styled.div`
  font-family: "Barlow", sans-serif;
  text-align: left;
  background: linear-gradient(
    40deg,
    ${(props) => gradient(props.color, { h: 0, s: 15, l: 0 })} 0%,
    ${(props) => gradient(props.color, { h: -30, s: 15, l: 18 })} 100%
  );
  border-radius: 8px;
  padding: 12px;
  position: relative;
  box-shadow: -4px 4px 8px #00000040;
  cursor: pointer;
  transition: 0.3s ease-out;
  overflow: hidden;
  animation: 0.2s ${fadeAnimation};

  &:hover {
    box-shadow: -4px 4px 12px #00000030;
    transform: scale(1.1);
    transition: 0.2s ease-out;
  }
`;

const Number = styled.p`
  font-family: "Lato", sans-serif;
  color: #0008;
  font-style: italic;
  font-weight: 300;
  font-size: 0.9em;
  margin-bottom: -0.4em;
`;

const Name = styled.h2`
  text-transform: capitalize;
  color: #000b;
  font-size: 1.1em;
  margin-bottom: 0;
`;

const Notch = styled.div`
  position: absolute;
  bottom: -23%;
  right: -23%;
  width: 46%;
  height: 46%;
  background: #00000080;
  transform: rotate(45deg);
`;

const Image = styled.img`
  width: 100%;
`;

class PokeCard extends React.Component {
  state = {
    starred: this.props.starred,
  };

  star = () => {
    star(this.props.username, this.props.name, this.state.starred).then(() =>
      this.setState({ starred: !this.state.starred * 1 })
    );
  };

  render() {
    return (
      <Location>
        {({ location }) => (
          <Card
            color={this.props.color}
            onClick={() =>
              // passes location to Router state
              // this allows routing logic to work
              navigate("/" + this.props.name, {
                state: { oldLocation: JSON.parse(JSON.stringify(location)) },
              })
            }
          >
            <Image alt={this.props.name} src={this.props.img} />
            <Number> #{("00" + this.props.id).slice(-3)} </Number>
            <Name> {this.props.name} </Name>
            {this.props.username && (
              <>
                <Notch />
                <PokeStar
                  onClick={(e) => {
                    e.stopPropagation();
                    this.star();
                  }}
                  starred={this.state.starred}
                  right="2.2%"
                  bottom="2.2%"
                  fontSize="1.5rem"
                />
              </>
            )}
          </Card>
        )}
      </Location>
    );
  }
}

export default PokeCard;
