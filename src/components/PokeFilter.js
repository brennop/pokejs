import React from 'react';
import styled from 'styled-components';
import {FaSearch} from 'react-icons/fa';

const Wrapper = styled.div`
  font-family: 'Barlow', sans-serif;
  position: relative;
  font-size: 1.2rem;
  background: ${props => props.theme.highlight};
  transition: 0.2s;
  color: #a0a0a0;
  width: 100%;
  border-radius: 6px;
  padding: 8px;

  display: flex;
  align-items: center;
`;

const Input = styled.input.attrs(props => ({
  value: props.value,
  onChange: props.onChange,
}))`

  border: none;
  background-color: inherit;
  color: #808080;
  width: 100%;
`;

const Search = styled(FaSearch)`
  margin: 0 12px;
`;

class PokeFilter extends React.Component {
  state = {
    value: this.props.value,
  };

  handleChange = event => {
    this.setState({value: event.target.value});
    this.props.onChange(event.target.value);
  };

  render() {
    return (
      <Wrapper>
        <Search />
        <Input
          type="text"
          value={this.state.value}
          onChange={this.handleChange}
        />
      </Wrapper>
    );
  }
}

export default PokeFilter;
