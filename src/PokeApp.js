import React from 'react';
import {ThemeProvider} from 'styled-components';
import {Location, Router} from '@reach/router';
import {dark, light} from './theme';
import PokeHome from './pages/PokeHome';
import PokeUser from './pages/PokeUser';
import PokeInfo from './pages/PokeInfo';
import PokeModal from './components/PokeModal';
import PokeHeader from './components/PokeHeader';

// Modal with routing using
// https://simon.lc/build-a-modal-route-with-reach-ui-and-reach-router
// implementation
function Routes(props) {
  return (
    <Router {...props}>
      <PokeHome path="/" />
      <PokeUser path="/user" />
      <PokeInfo path="/:pokemon" />
    </Router>
  );
}

class PokeApp extends React.Component {
  state = {
    theme: JSON.parse(localStorage.getItem('theme')) || light,
  };

  toggleTheme = () => {
    this.setState({theme: this.state.theme === light ? dark : light}, () =>
      localStorage.setItem('theme', JSON.stringify(this.state.theme)),
    );
  };

  render() {
    return (
      <Location>
        {({location, navigate}) => {
          // if state is empty, then it used url
          const {oldLocation} = location.state || {};
          return (
            <ThemeProvider theme={this.state.theme}>
              <div>
                <PokeHeader toggleTheme={this.toggleTheme}>
                  <Routes
                    location={oldLocation != null ? oldLocation : location}
                  />
                </PokeHeader>
                <PokeModal
                  show={oldLocation != null}
                  onHide={() => navigate(oldLocation.pathname)}>
                  <Routes location={location} />{' '}
                </PokeModal>
              </div>
            </ThemeProvider>
          );
        }}
      </Location>
    );
  }
}

export default PokeApp;
