//
// Color related functions and constants
//
import {zip} from 'lodash-es';

export const colors = {
  normal: [59, 21, 57],
  fire: [26, 85, 56],
  water: [221, 82, 66],
  electric: [48, 93, 57],
  grass: [98, 52, 54],
  ice: [177, 47, 72],
  fighting: [2, 66, 46],
  poison: [301, 45, 44],
  ground: [43, 68, 64],
  flying: [256, 81, 76],
  psychic: [342, 93, 65],
  bug: [67, 75, 41],
  rock: [50, 54, 46],
  ghost: [266, 27, 47],
  dragon: [257, 97, 60],
  dark: [24, 23, 36],
  steel: [240, 19, 76],
  fairy: [330, 50, 68],
};

export const blend = colors => {
  return zip(...colors).map(c => c.reduce((a, c) => a + c, 0) / colors.length);
};

export const blendTypes = types => {
  return blend(types.map(type => colors[type]));
};

// changes color using deltas
export const gradient = (color, delta) => {
  let [h, s, l] = color;
  h += delta.h;
  s += delta.s;
  l += delta.l;
  return `hsl(${h % 360}, ${s}%, ${l}%)`;
};
