export const filter = (obj, match, fields) => {
  return obj.filter(el => {
    return (
      Object.entries(el)
        .reduce(
          (string, [field, value]) =>
            fields.includes(field) ? string + value : string,
          '',
        )
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .indexOf(
          match
            .toLowerCase()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, ''),
        ) !== -1
    );
  });
};
