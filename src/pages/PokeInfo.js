import React from 'react';
import styled from 'styled-components';
import {analyse} from '../api';
import {capitalize} from '../utils/string';
import PokeStar from '../components/PokeStar';
import {star, getTrainer} from '../api';
import PokeBall from '../components/PokeBall';

const colors = require('../utils/colors.json');

const Container = styled.div`
  font-family: 'Barlow', sans-serif;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 500px;
  margin: auto;
  position: relative;
  background: ${props => props.theme.body};
  color: ${props => props.theme.text};
  transition: 0.2s;
`;

const Title = styled.h1`
  margin: none;
  font-weight: normal;
`;

const Type = styled.div`
  background: ${props => props.color};
  border-radius: 3px;
  color: white;
  text-align: center;
  font-size: 0.85em;
  font-weight: 600;
  margin: 4px;
  padding: 4px;
  display: inline;
`;

const Attr = styled.span`
  text-transform: uppercase;
  font-weight: 600;
  font-size: 0.95rem;
`;

const Image = styled.img`
  width: 75%;
`;
const Info = styled.div`
  margin-top: 12px;
`;

class PokeInfo extends React.Component {
  state = {
    username: localStorage.getItem('user'),
    isLoading: true,
  };

  _isMounted = false;

  componentDidMount() {
    this._isMounted = true;

    analyse(this.props.pokemon)
      .then(response => response.data)
      .then(
        data => this._isMounted && this.setState({...data, isLoading: false}),
      )
      .then(() => {
        if (this.state.name == null) {
          this.setState({
            ...require('../assets/missingno.json'),
            isLoading: false,
          });
        }
      });

    // check if pokemons is starred
    if (this.state.username) {
      getTrainer(this.state.username).then(response =>
        this.setState({
          starred: response.data.pokemons.map(x => x.name).indexOf(this.props.pokemon) + 1,
        }),
      );
    }
  }

  componentWillUnmount() {
    // prevent setState on unmounted component
    this._isMounted = false;
  }

  star = () => {
    star(this.state.username, this.state.name, this.state.starred)
      .then(() => this.setState({starred: !this.state.starred * 1}))
      .catch();
  };

  render() {
    if (this.state.isLoading)
      return (
        <div style={{textAlign: 'center'}}>
          <PokeBall />
        </div>
      );
    return (
      <Container>
        {this.state.username && (
          <PokeStar
            starred={this.state.starred}
            onClick={this.star}
            right="28px"
            top="28px"
            fontSize="2rem"
          />
        )}
        <Image src={this.state.image_url} alt={this.props.name} />
        <Title>
          #{('00' + this.state.id).slice(-3)} {capitalize(this.state.name)}
        </Title>
        <Info>
          <Attr>height: </Attr>
          {this.state.height}m <Attr>weight:</Attr> {this.state.weight}kg
        </Info>
        <Info>
          {this.state.kind.split(';').map(type => (
            <Type key={type} color={colors[type]}>
              {capitalize(type)}
            </Type>
          ))}
        </Info>
      </Container>
    );
  }
}

export default PokeInfo;
