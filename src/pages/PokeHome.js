import React from "react";
import styled from "styled-components";
import { catchEmAll, getTrainer } from "../api";
import { blendTypes } from "../utils/colors";
import PokeCard from "../components/PokeCard";
import PokeFilter from "../components/PokeFilter";
import PokeBall from "../components/PokeBall";
import { filter } from "../utils/filter";

const cardWidth = 144;
const gridGap = 20;

const Page = styled.div`
  text-align: center;
  padding: 20px 5%;
  background: ${(props) => props.theme.body};
  color: ${(props) => props.theme.text};
  min-height: 100vh;
  transition: 0.2s;
`;

const Grid = styled.div`
  position: relative;
  top: ${(props) => props.offset}px;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(9rem, 1fr));
  grid-gap: 20px;
  padding: 20px 0;
`;

const Footer = styled.div`
  text-align: center;
  height: 140px;
`;

class PokeList extends React.Component {
  state = {
    showModal: false,
    current: null,
    loaded: 0,
    isLoading: false,
    hasMore: true,
    pokemons: [],
    username: localStorage.getItem("user"),
    starred: [],
    search: "",
    error: null,
    offset: 0,
    cardHeight: 200,
  };

  grid = React.createRef();

  loadPokemons = () => {
    this.setState({ isLoading: true }, () => {
      // pega os pokemons
      catchEmAll(this.state.loaded + 1)
        .then((response) => {
          const newPokemons = response.data.data;
          const hasMore = response.data.next_page !== null;
          this.setState({
            loaded: this.state.loaded + 1,
            isLoading: false,
            hasMore: hasMore,
            pokemons: [...this.state.pokemons, ...newPokemons],
          });
        })
        .catch((error) => this.setState({ error: error }));
    });
  };

  componentDidMount() {
    // loads first pokemons
    this.loadPokemons();

    // get starred pokemons
    if (this.state.username) {
      getTrainer(this.state.username).then((response) =>
        this.setState({ starred: response.data.pokemons })
      );
    }

    // sets infinite scroll event
    setInterval(() => {
      const {
        loadPokemons,
        state: { isLoading, hasMore, error },
      } = this;

      if (error || isLoading || !hasMore) return;

      if (
        window.innerHeight + document.documentElement.scrollTop ===
          document.documentElement.offsetHeight ||
        this.state.pokemons.length < 100
      ) {
        loadPokemons();
      }
    }, 100);

    const resizeListener = () => {
      const perRow = Math.ceil(
        (this.grid.current.clientWidth - cardWidth) / (cardWidth + gridGap)
      );
      this.setState({ perRow });
    };

    // attaches a window width listener
    window.addEventListener("resize", resizeListener);
    resizeListener();

    window.addEventListener("scroll", () => {
      const cardHeight =
        (this.grid.current.children.length === 0
          ? 200
          : this.grid.current.children[0].clientHeight) + 20;
      const offset = Math.max(
        Math.floor((window.scrollY - 196) / cardHeight) - 1,
        0
      );
      this.setState({ offset, cardHeight });
    });
  }

  render() {
    const pokemons = filter(this.state.pokemons, this.state.search, [
      "name",
      "kind",
    ]);

    const { offset, perRow, cardHeight } = this.state;

    return (
      <Page>
        <PokeFilter
          value=""
          onChange={(search) => {
            this.setState({ search: search, currentCard: 1 });
          }}
        />
        {pokemons && (
          <div
            style={{
              height: Math.ceil(pokemons.length / perRow) * cardHeight + "px",
            }}
          >
            <Grid ref={this.grid} offset={offset * cardHeight}>
              {pokemons &&
                pokemons
                  .slice(offset * perRow, offset * perRow + perRow * 5)
                  .map((pokemon) => (
                    <PokeCard
                      key={pokemon.name}
                      name={pokemon.name}
                      id={pokemon.id}
                      img={pokemon.image_url}
                      color={blendTypes(pokemon.kind.split(";"))}
                      types={pokemon.kind.split(";")}
                      starred={
                        this.state.starred
                          .map((x) => x.name)
                          .indexOf(pokemon.name) + 1
                      }
                      username={this.state.username}
                    />
                  ))}
            </Grid>
          </div>
        )}
        <Footer>
          {this.state.error ? (
            <p> Unable to fetch Pokemons </p>
          ) : (
            this.state.isLoading && <PokeBall />
          )}
        </Footer>
      </Page>
    );
  }
}

export default PokeList;
