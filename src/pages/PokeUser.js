import React from 'react';
import styled from 'styled-components';
import {Form, Button} from 'react-bootstrap';
import {navigate} from '@reach/router';
import {signUp, getTrainer} from '../api';

const Container = styled.div`
  font-family: 'Barlow', sans-serif;
  max-width: 500px;
  display: flex;
  flex-direction: column;
  margin: auto;
  background: ${props => props.theme.body};
  color: ${props => props.theme.text}
`;

const Title = styled.h1`
  font-weight: 900;
  margin-bottom: 0.7em;
`;

class PokeUser extends React.Component {
  state = {
    error: null,
    currentUser: localStorage.getItem('user'),
    type: 'Login',
  };

  signUp = event => {
    event.preventDefault();
    const user = event.target.elements[0].value;
    signUp(user)
      .then(() => this.setState({type: 'Login'}))
      .catch(error => this.setState({error: error.response.data.username}));
  };

  login = event => {
    event.preventDefault();
    const user = event.target.elements[0].value;
    getTrainer(user)
      .then(() => {
        localStorage.setItem('user', user);
        /* reload page after login/out */
        setTimeout(() => window.location.reload(), 0);
        navigate('/');
      })
      .catch(error => this.setState({error: 'Invalid username!'}));
  };

  logout = () => {
    localStorage.removeItem('user');
    // this.setState({currentUser: null, type: 'Login'});
    setTimeout(() => window.location.reload(), 0);
    navigate('/');
  };

  handleChange = () => {
    this.setState({type: this.state.type === 'Login' ? 'Sign Up' : 'Login', error: null});
  };

  render() {
    const title = this.state.currentUser || this.state.type;
    const changeMessage =
      this.state.type === 'Login'
        ? 'Create a new account'
        : 'Already an user? Login';

    return (
      <Container>
        <Title> {title} </Title>
        {this.state.currentUser ? (
          <Button variant="danger" onClick={this.logout}>
            Logout
          </Button>
        ) : (
          <>
            <Form
              onSubmit={this.state.type === 'Login' ? this.login : this.signUp}>
              <Form.Group>
                <Form.Control
                  type="user"
                  placeholder="Username"
                  isInvalid={this.state.error}
                />
                <Form.Control.Feedback type="invalid">
                  {this.state.error}
                </Form.Control.Feedback>
              </Form.Group>
              <Button type="submit"> {title} </Button>
            </Form>
            <Button variant="link" onClick={this.handleChange}>
              {changeMessage}
            </Button>
          </>
        )}
      </Container>
    );
  }
}

export default PokeUser;
