export const light = {
  name: 'light',
  body: '#fafafa',
  text: '#232323',
  highlight: '#eee',
}

export const dark = {
  name: 'dark',
  body: '#191919',
  text: '#e7e7e7',
  highlight: '#222',
}
