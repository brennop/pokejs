# Pokejs

Pokedex implementada usando React.

## Uso

Use `yarn build` para visualizar o projeto otimizado. A aplicação possui muitos
cards e a performance pode ser afetada se iniciado com `yarn start`.

## Bibliotecas

- React
- [axios](https://github.com/axios/axios)
- [Reach Router](https://github.com/reach/router)
- [React Bootstrap](https://react-bootstrap.github.io/)
- [styled-components](https://www.styled-components.com/)
- [lodash](https://lodash.com/)
- [react-icons](https://react-icons.netlify.com/#/)
- [react-animations](https://github.com/FormidableLabs/react-animations)

## Funcionalidades

- Página principal com os Pokémons
- Infinite Scroll
- Barra para filtragem
- Routing dinâmico (estilo Instagram)
- Favoritar Pokémons
- Dark mode
- Página de 404

## Bugs
- setState quando o Modal é fechado. Isso ocorre por causa da animação do
    bootstrap.
